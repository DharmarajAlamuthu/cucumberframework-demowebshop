package StepDefinations;

import org.junit.runner.RunWith;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.CucumberOptions.SnippetType;
//@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"/home/dharmaraj/Downloads/cucumber/DemoWebShop_Assiginment/src/test/resources/Feature"},
		glue = "StepDefinations",
		snippets = SnippetType.CAMELCASE,
		monochrome = true
		//dryRun = true
	   )
public class RunnerClasss extends AbstractTestNGCucumberTests {
}
